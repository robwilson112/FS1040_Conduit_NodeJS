FROM node:12.16.3-slim


WORKDIR /app


COPY package*.json ./


RUN npm install


COPY ./ ./

RUN chmod +x wait-for-it.sh

# as per backend api
CMD npm run start