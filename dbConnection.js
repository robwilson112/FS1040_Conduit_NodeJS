const {Sequelize} = require('sequelize')

// LOCAL CONNECTION
// const sequelize = new Sequelize('new_db','new_user','new_password',{
//     dialect: 'mysql',
//     host:'db',
//     logging: false
// }); 


//AMAZON RDS CONNECTION
/* const sequelize = new Sequelize('conduit1',process.env.USER_NAME,process.env.PASSWORD,{
    dialect: 'mysql',
    host:process.env.DB_HOST,
    logging: false,
    port: 3306
});
 */

// Production updated
const sequelize = new Sequelize('new_db',process.env.DATABASE_USER || 'new_user',process.env.DATABASE_PASSWORD || 'new_password',{
    dialect: 'mysql',
    host: process.env.DATABASE_HOST || 'db',
    socketPath: process.env.DATABASE_SOCKET || '/cloudsql/fs1040-final-project:us-central1:courseproject',
    logging: false,
    port: 3306,
    dialectOptions: {
        ssl: {
            require: true,
            rejectUnauthorized: false // <<<<<<< YOU NEED THIS
        }
    }
});

const checkConnection =async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}

checkConnection()

module.exports = sequelize